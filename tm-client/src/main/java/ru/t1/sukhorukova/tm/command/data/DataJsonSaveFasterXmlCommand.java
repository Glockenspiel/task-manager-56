package ru.t1.sukhorukova.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.sukhorukova.tm.dto.request.data.DataJsonSaveFasterXmlRequest;

@Component
public final class DataJsonSaveFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-save-json";

    @NotNull
    public static final String DESCRIPTION = "Save data in json file.";

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA SAVE JSON]");

        getDomainEndpoint().jsonSaveFasterXmlData(new DataJsonSaveFasterXmlRequest(getToken()));
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}

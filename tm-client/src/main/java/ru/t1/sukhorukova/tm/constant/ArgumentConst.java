package ru.t1.sukhorukova.tm.constant;

import org.jetbrains.annotations.NotNull;

public final class ArgumentConst {

    @NotNull
    public static final String CMD_HELP = "-h";

    @NotNull
    public static final String CMD_VERSION = "-v";

    @NotNull
    public static final String CMD_ABOUT = "-a";

    @NotNull
    public static final String CMD_INFO = "-i";

    @NotNull
    public static final String CMD_ARGUMENT = "-arg";

    @NotNull
    public static final String CMD_COMMAND = "-cmd";

}

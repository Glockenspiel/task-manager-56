package ru.t1.sukhorukova.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.sukhorukova.tm.dto.request.data.DataBinarySaveRequest;

@Component
public final class DataBinarySaveCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-save-bin";

    @NotNull
    public static final String DESCRIPTION = "Save data to binary file.";

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA SAVE BINARY]");

        getDomainEndpoint().binarySaveData(new DataBinarySaveRequest(getToken()));
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}

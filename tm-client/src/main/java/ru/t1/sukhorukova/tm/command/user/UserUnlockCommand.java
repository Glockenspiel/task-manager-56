package ru.t1.sukhorukova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.sukhorukova.tm.dto.request.user.UserUnlockRequest;
import ru.t1.sukhorukova.tm.enumerated.Role;
import ru.t1.sukhorukova.tm.util.TerminalUtil;

@Component
public final class UserUnlockCommand extends AbstractUserCommand {

    @NotNull private final String NAME = "user-unlock";
    @NotNull private final String DESCRIPTION = "User unlock.";

    @Override
    public void execute() {
        System.out.println("[USER LOCK]");

        System.out.println("Enter login:");
        @Nullable final String login = TerminalUtil.nextLine();

        @NotNull final UserUnlockRequest request = new UserUnlockRequest(getToken());
        request.setLogin(login);
        getUserEndpoint().unlockUser(request);
    }

    @NotNull @Override
    public String getName() {
        return NAME;
    }

    @NotNull @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull @Override
    public Role[] getRoles() {
        return new Role[] { Role.ADMIN };
    }

}

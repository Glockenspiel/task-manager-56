package ru.t1.sukhorukova.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.sukhorukova.tm.dto.request.data.DataYamlSaveFasterXmlRequest;

@Component
public final class DataYamlSaveFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-save-yaml";

    @NotNull
    public static final String DESCRIPTION = "Save data in yaml file.";

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA SAVE YAML]");

        getDomainEndpoint().yamlSaveFasterXmlData(new DataYamlSaveFasterXmlRequest(getToken()));
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}

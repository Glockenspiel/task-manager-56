package ru.t1.sukhorukova.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.sukhorukova.tm.api.endpoint.*;
import ru.t1.sukhorukova.tm.api.repository.ICommandRepository;
import ru.t1.sukhorukova.tm.api.service.*;
import ru.t1.sukhorukova.tm.command.AbstractCommand;
import ru.t1.sukhorukova.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.sukhorukova.tm.exception.system.CommandNotSupportedException;
import ru.t1.sukhorukova.tm.repository.CommandRepository;
import ru.t1.sukhorukova.tm.service.*;
import ru.t1.sukhorukova.tm.util.SystemUtil;
import ru.t1.sukhorukova.tm.util.TerminalUtil;

import javax.annotation.PostConstruct;
import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

@Component
public final class Bootstrap implements ILocatorService {

    @NotNull
    private static final String PACKAGE_COMMANDS = "ru.t1.sukhorukova.tm.command";

    @Getter
    @NotNull
    @Autowired
    private ICommandService commandService;

    @Getter
    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @Getter
    @NotNull
    @Autowired
    private ITokenService tokenService;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private FileScanner fileScanner;

    @Getter
    @NotNull
    @Autowired
    private ISystemEndpoint systemEndpoint;

    @Getter
    @NotNull
    @Autowired
    private IDomainEndpoint domainEndpoint;

    @Getter
    @NotNull
    @Autowired
    private IProjectEndpoint projectEndpoint;

    @Getter
    @NotNull
    @Autowired
    private ITaskEndpoint taskEndpoint;

    @Getter
    @NotNull
    @Autowired
    private IAuthEndpoint authEndpoint;

    @Getter
    @NotNull
    @Autowired
    private IUserEndpoint userEndpoint;

    @SneakyThrows
    private void registry(@NotNull final Class<? extends AbstractCommand> clazz) {
        if (Modifier.isAbstract(clazz.getModifiers())) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        final AbstractCommand command = clazz.newInstance();
        registry(command);
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setLocatorService(this);
        commandService.add(command);
    }

    public void start(@NotNull final String[] args) {
        @NotNull final Reflections reflections = new Reflections(PACKAGE_COMMANDS);
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes)
            registry(clazz);
        fileScanner.setBootstrap(this);

        initPID();
        initLogger();
        processArguments(args);
        initFileScanner();
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                @NotNull final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.out.println("[FAIL]");
            }
        }
    }

    private void processArguments(@Nullable final String[] args) {
        try {
            if (args == null || args.length == 0) return;
            processArgument(args[0]);
            System.out.println("[OK]");
            System.exit(0);
        } catch (@NotNull final RuntimeException e) {
            System.out.println(e.getMessage());
            System.out.println("[FAIL]");
            System.exit(1);
        }
    }

    private void processArgument(@Nullable final String arg) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(arg);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(arg);
        abstractCommand.execute();
    }

    public void processCommand(@Nullable final String command) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        abstractCommand.execute();
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
            }
        });
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void initFileScanner() {
        fileScanner.init();
    }

}

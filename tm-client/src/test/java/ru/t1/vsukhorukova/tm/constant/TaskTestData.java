package ru.t1.vsukhorukova.tm.constant;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;

@UtilityClass
public final class TaskTestData {

    @NotNull
    public final static String TASK1_NAME = "TASK_01";

    @NotNull
    public final static String TASK1_DESC = "DESCRIPTION_01";

    @NotNull
    public final static String TASK2_NAME = "TASK_02";

    @NotNull
    public final static String TASK2_DESC = "DESCRIPTION_02";

    @NotNull
    public final static String TASK3_NAME = "TASK_03";

    @NotNull
    public final static String TASK3_DESC = "DESCRIPTION_03";

}

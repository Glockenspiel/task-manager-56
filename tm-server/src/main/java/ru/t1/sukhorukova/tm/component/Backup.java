package ru.t1.sukhorukova.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.sukhorukova.tm.api.service.IDomainService;
import ru.t1.sukhorukova.tm.service.DomainService;

import java.nio.file.Files;
import java.nio.file.Paths;

@Component
public final class Backup extends Thread {

    @NotNull
    @Autowired
    private IDomainService domainService;

    public Backup() {
        this.setDaemon(true);
    }

    public void init() {
        start();
    }

    public void save() {
        domainService.dataBackupSave();
    }

    public void load() {
        if (!Files.exists(Paths.get(DomainService.FILE_BACKUP))) return;
        domainService.dataBackupLoad();
    }

    @Override
    @SneakyThrows
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            Thread.sleep(3000);
            save();
        }
    }

}

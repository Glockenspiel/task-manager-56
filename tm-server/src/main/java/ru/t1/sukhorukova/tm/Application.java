package ru.t1.sukhorukova.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.sukhorukova.tm.component.Bootstrap;
import ru.t1.sukhorukova.tm.config.ServerConfiguration;

public class Application {

    public static void main(@Nullable final String[] args) {
        @NotNull final ApplicationContext context = new AnnotationConfigApplicationContext(ServerConfiguration.class);
        @NotNull final Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.start(args);
    }

}

package ru.t1.sukhorukova.tm.service.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import ru.t1.sukhorukova.tm.api.repository.dto.ISessionDtoRepository;
import ru.t1.sukhorukova.tm.api.service.dto.ISessionDtoService;
import ru.t1.sukhorukova.tm.dto.model.SessionDTO;
import ru.t1.sukhorukova.tm.repository.dto.SessionDtoRepository;

import javax.persistence.EntityManager;

@Service
public final class SessionDtoService extends AbstractUserOwnedDtoService<SessionDTO, ISessionDtoRepository> implements ISessionDtoService {

    public ISessionDtoRepository getRepository() {
        return context.getBean(SessionDtoRepository.class);
    }

}

package ru.t1.sukhorukova.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.dto.model.AbstractModelDTO;

import javax.persistence.EntityManager;
import java.util.List;

public interface IDtoRepository<M extends AbstractModelDTO> {

    EntityManager getEntityManager();

    void add(@NotNull M model);

    void update(@NotNull M model);

    void remove(@NotNull M model);

    @Nullable
    List<M> findAll();

    @Nullable
    M findOneById(@NotNull String id);

    void removeOneById(@NotNull String id);

    void removeAll();

    long getSize();

}

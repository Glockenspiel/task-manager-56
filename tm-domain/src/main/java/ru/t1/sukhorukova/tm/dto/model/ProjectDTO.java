package ru.t1.sukhorukova.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.api.model.IWBS;
import ru.t1.sukhorukova.tm.enumerated.Status;
import ru.t1.sukhorukova.tm.exception.entity.UserNotFoundException;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@Setter
@Getter
@Entity
@NoArgsConstructor
@Table(name = "TM_PROJECT")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class ProjectDTO extends AbstractUserOwnerModelDTO implements IWBS {

    @NotNull
    @Column(name = "NAME")
    private String name = "";

    @NotNull
    @Column(name = "DESCRIPTION")
    private String description = "";

    @NotNull
    @Column(name = "STATUS")
    private Status status = Status.NOT_STARTED;

    @NotNull
    @Column(name = "CREATED")
    private Date created = new Date();

    public ProjectDTO(@Nullable final UserDTO user,
                      @NotNull final String name,
                      @NotNull final String description,
                      @NotNull final Status status
    ) {
        if (user == null) throw new UserNotFoundException();
        this.setUserId(user.getId());
        this.name = name;
        this.description = description;
        this.status = status;
    }

}

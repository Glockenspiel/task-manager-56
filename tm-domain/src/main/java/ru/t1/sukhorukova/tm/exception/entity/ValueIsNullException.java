package ru.t1.sukhorukova.tm.exception.entity;

public final class ValueIsNullException extends AbstractEntityNotFoundException {

    public ValueIsNullException() {
        super("Error! This value is null...");
    }

}
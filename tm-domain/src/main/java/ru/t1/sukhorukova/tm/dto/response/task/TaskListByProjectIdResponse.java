package ru.t1.sukhorukova.tm.dto.response.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.dto.model.TaskDTO;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public final class TaskListByProjectIdResponse extends AbstractTaskResponse {

    @Nullable
    private List<TaskDTO> tasks;

}

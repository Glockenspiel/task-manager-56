package ru.t1.sukhorukova.tm.dto.response.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.dto.response.AbstractResultResponse;

@Getter
@Setter
@NoArgsConstructor
public final class UserLoginResponse extends AbstractResultResponse {

    @Nullable
    private String token;

    public UserLoginResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

    public UserLoginResponse(@NotNull final String token) {
        this.token = token;
    }

}
